<?php
    define("HOST", "localhost");
    define("USERNAME", "root");
    define("PASSWORD", "");
    define("DBNAME", "db_credit_transfer");

    $link = mysqli_connect(HOST, USERNAME, PASSWORD, DBNAME);

    if(!$link){
        die("Connection Problem : ".mysqli_error($link));
    }